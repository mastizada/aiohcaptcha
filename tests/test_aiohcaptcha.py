import json
from unittest import mock

import pytest

from aiohcaptcha import HCaptchaClient, __version__

TESTING_SECRET_KEY = "0x0000000000000000000000000000000000000000"
TESTING_SITE_KEY = "10000000-ffff-ffff-ffff-000000000001"


def test_version():
    assert __version__ == "0.1.2"


class MockedResponse:
    def __init__(self, contents, status_code=200):
        self._contents = contents
        self.status = status_code

    async def text(self):
        return json.dumps(self._contents)

    async def json(self):
        return self._contents

    async def __aexit__(self, exc_type, exc, tb):
        pass

    async def __aenter__(self):
        return self


@pytest.mark.asyncio
async def test_verification():
    with pytest.raises(RuntimeError):
        HCaptchaClient()
    client = HCaptchaClient(TESTING_SECRET_KEY)
    # manual captcha verification is not possible
    assert await client.verify(TESTING_SITE_KEY) is False, client.response.error_codes


@pytest.mark.asyncio
async def test_debug_mode():
    client = HCaptchaClient(TESTING_SECRET_KEY, debug=True)
    assert await client.verify(TESTING_SITE_KEY, sitekey=TESTING_SITE_KEY)
    assert await client.verify(TESTING_SITE_KEY, sitekey="12345") is False


@pytest.mark.asyncio
@mock.patch("aiohttp.ClientSession.post")
async def test_correct_response(mock_post):
    mock_post.return_value = MockedResponse(contents={"success": True})
    client = HCaptchaClient(TESTING_SECRET_KEY)
    result = await client.verify(TESTING_SITE_KEY.replace("0001", "1001"))
    assert result is True


@pytest.mark.asyncio
async def test_incorrect_response():
    client = HCaptchaClient(TESTING_SECRET_KEY)
    result = await client.verify(TESTING_SITE_KEY.replace("0001", "1001"))
    assert result is False
    assert len(client.response.error_codes) == 1
    assert "invalid-input-response" in client.response.error_codes


@pytest.mark.asyncio
async def test_incorrect_response_code():
    client = HCaptchaClient(TESTING_SECRET_KEY, api_url="https://pypi.org")
    result = await client.verify(TESTING_SITE_KEY, remote_ip="127.0.0.1")
    assert result is False
    assert client.response is None
